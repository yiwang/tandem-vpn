/*
 * transfer.h
 *
 *  Created on: 2015年5月25日
 *      Author: Yi
 */

#ifndef SRC_TRANSFER_H_
#define SRC_TRANSFER_H_


typedef void (*recvfn)(void *buf);

void send(void *msg, const char *expect, recvfn recv);

#endif /* SRC_TRANSFER_H_ */
