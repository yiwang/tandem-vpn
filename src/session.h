/*
 * session.h
 *
 *  Created on: 2015年6月1日
 *      Author: Yi
 */

#ifndef SRC_SESSION_H_
#define SRC_SESSION_H_

#define DEVICE_ID_LEN       20
#define MAX_SDP_LEN         2048

#define MSG_REGISTER        0x01
#define MSG_REGISTER_ACK    0x02
#define MSG_KEEPALIVE       0x03
#define MSG_OFFER           0x04
#define MSG_ANSWER          0x05
#define UNREGISTER          0x06

int sess_taking_init(vpn_ctx_t *ctx);
int sess_slave_offer(vpn_ctx_t *ctx);
int sess_talking_stop(vpn_ctx_t *ctx);
int sess_master_answer(
		const tandemvpn_options_t *opts);

void sess_route_init(vpn_ctx_t *ctx);
void sess_route_uninit(vpn_ctx_t *ctx);

#endif /* SRC_SESSION_H_ */
