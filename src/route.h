/*
 * route.h
 *
 *  Created on: 2015年5月27日
 *      Author: Yi
 */

#ifndef SRC_ROUTE_H_
#define SRC_ROUTE_H_


int route(int add, const char *target, const char *gw, const char *mask);
int get_default_gw(char gw[], int size);


#endif /* SRC_ROUTE_H_ */
