/*
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/queue.h>
#include <fcntl.h>
#include <unistd.h>
#include <pthread.h>

#include <strings.h> // fix bzero()
#include <pjlib.h>
#include <pjlib-util.h>
#include <pjnath.h>

#include <libHX/defs.h>
#include <libHX/deque.h>

#include "log.h"
#include "args.h"
#include "vpn.h"
#include "crypto.h"
#include "ifc.h"
#include "session.h"
#include "control.h"
#include "iceagent.h"

#define THIS_MODULE             "ICE-Agent"
#define KEEPALIVE_INTERVAL      15
#define INVALID_SOCKET 			-1

#ifndef TARGET_WIN32
static int max(int a, int b) {
	return a > b ? a : b;
}
#endif

/* This is agent */
struct _agent {
	struct options {
		unsigned int    comp_cnt;
		int	            max_host;
		pj_bool_t       regular;
		pj_str_t        turn_srv;
		int 			turn_port;
		pj_bool_t       turn_tcp;
		pj_str_t        turn_username;
		pj_str_t        turn_password;
		pj_bool_t       turn_fingerprint;
	} opt;

	pj_caching_pool	    cp;
	pj_pool_t		    *pool;
	pj_thread_t		    *thread;
	pj_bool_t		    quit_flag;
	pj_ice_strans_cfg	ice_cfg;
	pj_ice_strans	    *ice_strans;
	pj_bool_t           init;
	pj_status_t         init_status;

	pj_bool_t           negotiation;
	pj_status_t         negotiation_status;

	vpn_ctx_t * 		vpn_ctx;

	time_t				last_recv;

	/* Variables to store parsed remote ICE info */
	struct remote_info {
		char		    ufrag[80];
		char		    pwd[80];
		unsigned	    comp_cnt;
		pj_sockaddr	    def_addr[PJ_ICE_MAX_COMP];
		unsigned	    cand_cnt;
		pj_ice_sess_cand cand[PJ_ICE_ST_MAX_CAND];
	} remote;
} agent;


struct route_t {
	const char *intf;
	pj_sockaddr target_ip;
	int prefix_length;
	struct in_addr gateway_ip;
};

static struct HXdeque *route_queue = NULL;


static void ice_route_init();
static void ice_route_uninit();

static pj_status_t agent_init_instance(void);
static void agent_destroy_instance(void);

/* Utility to display error messages */
static void agent_perror(const char *title, pj_status_t status)
{
	char errmsg[PJ_ERR_MSG_SIZE];

	pj_strerror(status, errmsg, sizeof(errmsg));
	errf("[%d] %s: %s", status, title, errmsg);
}

#define CHECK(expr)	status=expr; \
		if (status!=PJ_SUCCESS) { \
			agent_perror(#expr, status); \
			return status; \
		}

/*
 * This function checks for events from both timer and ioqueue (for
 * network events). It is invoked by the worker thread.
 */
static pj_status_t agent_handle_events(unsigned max_msec, unsigned *p_count)
{
	enum { MAX_NET_EVENTS = 1 };
	pj_time_val max_timeout = {0, 0};
	pj_time_val timeout = {0, 0};
	unsigned count = 0, net_event_count = 0;
	int c;

	max_timeout.msec = max_msec;

	/* Poll the timer to run it and also to retrieve the earliest entry. */
	timeout.sec = timeout.msec = 0;
	c = pj_timer_heap_poll(agent.ice_cfg.stun_cfg.timer_heap, &timeout);
	if (c > 0)
		count += c;

	/* timer_heap_poll should never ever returns negative value, or otherwise
	 * ioqueue_poll() will block forever!
	 */
	pj_assert(timeout.sec >= 0 && timeout.msec >= 0);
	if (timeout.msec >= 1000)
		timeout.msec = 999;

	/* compare the value with the timeout to wait from timer, and use the
	 * minimum value.
	 */
	if (PJ_TIME_VAL_GT(timeout, max_timeout))
		timeout = max_timeout;

	/* Poll ioqueue.
	 * Repeat polling the ioqueue while we have immediate events, because
	 * timer heap may process more than one events, so if we only process
	 * one network events at a time (such as when IOCP backend is used),
	 * the ioqueue may have trouble keeping up with the request rate.
	 *
	 * For example, for each send() request, one network event will be
	 *   reported by ioqueue for the send() completion. If we don't poll
	 *   the ioqueue often enough, the send() completion will not be
	 *   reported in timely manner.
	 */
	do {
		c = pj_ioqueue_poll( agent.ice_cfg.stun_cfg.ioqueue, &timeout);
		if (c < 0) {
			pj_status_t err = pj_get_netos_error();
			pj_thread_sleep(PJ_TIME_VAL_MSEC(timeout));
			if (p_count)
				*p_count = count;
			return err;
		} else if (c == 0) {
			break;
		} else {
			net_event_count += c;
			timeout.sec = timeout.msec = 0;
		}
	} while (c > 0 && net_event_count < MAX_NET_EVENTS);

	count += net_event_count;
	if (p_count)
		*p_count = count;

	return PJ_SUCCESS;
}

/*
 * This is the worker thread that polls event in the background.
 */
static int agent_worker_thread(void *unused)
{
	PJ_UNUSED_ARG(unused);

	while (!agent.quit_flag) {
		agent_handle_events(500, NULL);
	}

	return 0;
}

/*
 * This is the main application initialization function. It is called
 * once (and only once) during application initialization sequence by 
 * main().
 */
static pj_status_t agent_init(
		vpn_ctx_t *vpn_ctx,
		const char *turn_srv,
		int port,
		const char *user,
		const char *passwd)
{
	pj_status_t status;

	agent.opt.comp_cnt = 1;
	agent.opt.max_host = -1;
	agent.opt.regular = PJ_TRUE;
	agent.opt.turn_srv = pj_str((char *)turn_srv);
	agent.opt.turn_port = port;
	agent.opt.turn_username = pj_str((char *)user);
	agent.opt.turn_password = pj_str((char *)passwd);
	agent.opt.turn_fingerprint = PJ_TRUE;

	agent.vpn_ctx = vpn_ctx;

	agent.last_recv = time(NULL);

	/* Initialize the libraries before anything else */
	CHECK(pj_init());
	CHECK(pjlib_util_init());
	CHECK(pjnath_init());

	pj_log_set_level(6);

	/* Must create pool factory, where memory allocations come from */
	pj_caching_pool_init(&agent.cp, NULL, 0);

	/* Init our ICE settings with null values */
	pj_ice_strans_cfg_default(&agent.ice_cfg);

	agent.ice_cfg.stun_cfg.pf = &agent.cp.factory;

	/* Create application memory pool */
	agent.pool = pj_pool_create(&agent.cp.factory, THIS_MODULE,
			512, 512, NULL);

	/* Create timer heap for timer stuff */
	CHECK( pj_timer_heap_create(agent.pool, 100,
			&agent.ice_cfg.stun_cfg.timer_heap) );

	/* and create ioqueue for network I/O stuff */
	CHECK( pj_ioqueue_create(agent.pool, 16,
			&agent.ice_cfg.stun_cfg.ioqueue) );

	/* something must poll the timer heap and ioqueue,
	 * unless we're on Symbian where the timer heap and ioqueue run
	 * on themselves.
	 */
	CHECK(pj_thread_create(agent.pool, THIS_MODULE, &agent_worker_thread,
			NULL, 0, 0, &agent.thread));

	agent.ice_cfg.af = pj_AF_INET();

	/* -= Start initializing ICE stream transport config =- */

	/* Maximum number of host candidates */
	if (agent.opt.max_host != -1)
		agent.ice_cfg.stun.max_host_cands = agent.opt.max_host;

	/* Nomination strategy */
	if (agent.opt.regular)
		agent.ice_cfg.opt.aggressive = PJ_FALSE;
	else
		agent.ice_cfg.opt.aggressive = PJ_TRUE;

	/* Configure STUN/srflx candidate resolution */
	if (agent.opt.turn_srv.slen) {
		pj_sockaddr bound_addr;
		pj_sockaddr_init(pj_AF_INET(), &bound_addr, NULL, vpn_ctx->args->port);

		agent.ice_cfg.stun.server = agent.opt.turn_srv;
    	agent.ice_cfg.stun.port = agent.opt.turn_port;
		agent.ice_cfg.stun.cfg.bound_addr = bound_addr;
	    agent.ice_cfg.stun.cfg.ka_interval = KEEPALIVE_INTERVAL;
	}

	/* Configure TURN candidate */
	if (agent.opt.turn_srv.slen) {
		pj_sockaddr bound_addr;
		pj_sockaddr_init(pj_AF_INET(), &bound_addr, NULL, vpn_ctx->args->port + 1);

    	agent.ice_cfg.turn.server = agent.opt.turn_srv;
    	agent.ice_cfg.turn.port = agent.opt.turn_port;
    	agent.ice_cfg.stun.cfg.bound_addr = bound_addr;

		/* TURN credential */
		agent.ice_cfg.turn.auth_cred.type = PJ_STUN_AUTH_CRED_STATIC;
		agent.ice_cfg.turn.auth_cred.data.static_cred.username = agent.opt.turn_username;
		agent.ice_cfg.turn.auth_cred.data.static_cred.data_type = PJ_STUN_PASSWD_PLAIN;
		agent.ice_cfg.turn.auth_cred.data.static_cred.data = agent.opt.turn_password;

		/* Connection type to TURN server */
		if (agent.opt.turn_tcp)
			agent.ice_cfg.turn.conn_type = PJ_TURN_TP_TCP;
		else
			agent.ice_cfg.turn.conn_type = PJ_TURN_TP_UDP;

		agent.ice_cfg.turn.alloc_param.ka_interval = KEEPALIVE_INTERVAL;
	}

	agent.init = PJ_FALSE;
	agent.init_status = PJ_SUCCESS;

	agent.negotiation = PJ_FALSE;
	agent.negotiation_status = PJ_SUCCESS;

	/* -= That's it for now, initialization is complete =- */
	return PJ_SUCCESS;
}

/* Utility: display error message and exit application (usually
 * because of fatal error.
 */
static void agent_destroy()
{
	logf("ICE agent shutting down...");

	if (agent.ice_strans)
		pj_ice_strans_destroy(agent.ice_strans);

	pj_thread_sleep(1000);

	agent.quit_flag = PJ_TRUE;
	if (agent.thread) {
		pj_thread_join(agent.thread);
		pj_thread_destroy(agent.thread);
	}

	if (agent.ice_cfg.stun_cfg.ioqueue)
		pj_ioqueue_destroy(agent.ice_cfg.stun_cfg.ioqueue);

	if (agent.ice_cfg.stun_cfg.timer_heap)
		pj_timer_heap_destroy(agent.ice_cfg.stun_cfg.timer_heap);

	pj_pool_release(agent.pool);

	pj_caching_pool_destroy(&agent.cp);

	pj_shutdown();
}

/*
 * This is the callback that is registered to the ICE stream transport to
 * receive notification about incoming data. By "data" it means application
 * data such as RTP/RTCP, and not packets that belong to ICE signaling (such
 * as STUN connectivity checks or TURN signaling).
 */
static void agent_on_rx_data(
		pj_ice_strans *ice_st,
		unsigned comp_id,
		void *pkt, pj_size_t size,
		const pj_sockaddr_t *src_addr,
		unsigned src_addr_len)
{
	char ipstr[PJ_INET6_ADDRSTRLEN+10];
	pj_uint32_t len;

	PJ_UNUSED_ARG(ice_st);
	PJ_UNUSED_ARG(src_addr_len);
	PJ_UNUSED_ARG(pkt);

	// Don't do this! It will ruin the packet buffer in case TCP is used!
	//((char*)pkt)[size] = '\0';

	debugf("[%08x] ICE Component %d: received %ld bytes data from %s",
			(unsigned)pthread_self(), comp_id, size,
			pj_sockaddr_print(src_addr, ipstr, sizeof(ipstr), 3));

	agent.last_recv = time(NULL);

	if (ice_packet_is_ping((char *)pkt, size)) {
		ice_packet_pong();
		return;
	}

	recv_from_peer(agent.vpn_ctx, pkt, size);
}

/*
 * This is the callback that is registered to the ICE stream transport to
 * receive notification about ICE state progression.
 */
static void agent_on_ice_complete(pj_ice_strans *ice_st, 
		pj_ice_strans_op op,
		pj_status_t status)
{
	const char *opname;

	if (op == PJ_ICE_STRANS_OP_INIT) {
		agent.init = PJ_TRUE;
		agent.init_status = status;
		opname = "initialization";
	} else if (op == PJ_ICE_STRANS_OP_NEGOTIATION) {
		agent.negotiation = PJ_TRUE;
		agent.negotiation_status = status;
		opname = "negotiation";
	} else {
		opname =  "unknown_op";
	}

	if (status == PJ_SUCCESS) {
		logf("[%08x] ICE %s successful", (unsigned)pthread_self(), opname);
	} else {
		char errmsg[PJ_ERR_MSG_SIZE];
		pj_strerror(status, errmsg, sizeof(errmsg));
		errf("ICE %s failed: %s", opname, errmsg);
	}

	if (op == PJ_ICE_STRANS_OP_INIT) {
		if (status == PJ_SUCCESS) {
			ice_create_session(agent.vpn_ctx->args->mode == SHADOWVPN_MODE_CLIENT);

			if (agent.vpn_ctx->args->mode == SHADOWVPN_MODE_CLIENT) {
				control_req(CTRL_REQ_CONN, NULL);
			}
		}
		else {
			control_req(CTRL_UNINIT_ICE, NULL);
			control_req(CTRL_INIT_ICE, NULL);
		}
	}
	else if (op == PJ_ICE_STRANS_OP_NEGOTIATION) {
		if (status == PJ_SUCCESS) {
			control_req(CTRL_ICE_NEGO_DONE, NULL);
		}
		else {
			ice_destroy_session();
		    ice_create_session(agent.vpn_ctx->args->mode == SHADOWVPN_MODE_CLIENT);
		}
	}
	else {
		if (status != PJ_SUCCESS) {
			control_req(CTRL_UNINIT_ICE, NULL);
			control_req(CTRL_INIT_ICE, NULL);
		}
	}
}

/*
 * Create ICE stream transport instance.
 */
static pj_status_t agent_init_instance(void)
{
	pj_ice_strans_cb icecb;
	pj_status_t status;

	if (agent.ice_strans != NULL) {
		return PJ_EINVALIDOP;
	}

	/* init the callback */
	pj_bzero(&icecb, sizeof(icecb));
	icecb.on_rx_data = agent_on_rx_data;
	icecb.on_ice_complete = agent_on_ice_complete;

	/* create the instance */
	status = pj_ice_strans_create(THIS_MODULE,  /* object name  */
			&agent.ice_cfg,                 /* settings	    */
			agent.opt.comp_cnt,             /* comp_cnt	    */
			NULL,                           /* user data    */
			&icecb,	                        /* callback	    */
			&agent.ice_strans);             /* instance ptr */

	if (status != PJ_SUCCESS)
		agent_perror("Error creating ice instance", status);
	else
		logf("ICE instance successfully created");

	return status;
}

/* Utility to nullify parsed remote info */
static void agent_reset_remote(void)
{
	pj_bzero(&agent.remote, sizeof(agent.remote));
}

/*
 * Destroy ICE stream transport instance.
 */
static void agent_destroy_instance(void)
{
	if (agent.ice_strans == NULL) {
		return;
	}

	pj_ice_strans_destroy(agent.ice_strans);
	agent.ice_strans = NULL;

	agent_reset_remote();

	agent.init = PJ_FALSE;
	agent.init_status = PJ_SUCCESS;
	agent.negotiation = PJ_FALSE;
	agent.negotiation_status = PJ_SUCCESS;

	logf("ICE instance destroyed");
}

/*
 * Create ICE session.
 */
int ice_create_session(int is_slave)
{
	debugf("ice_create_session");

	pj_status_t status;
	pj_ice_sess_role role = (is_slave ?
			PJ_ICE_SESS_ROLE_CONTROLLING :
			PJ_ICE_SESS_ROLE_CONTROLLED);

	agent.init = PJ_FALSE;
	agent.init_status = PJ_SUCCESS;
	agent.negotiation = PJ_FALSE;
	agent.negotiation_status = PJ_SUCCESS;

	if (agent.ice_strans == NULL) {
		return PJ_EINVALIDOP;
	}

	if (pj_ice_strans_has_sess(agent.ice_strans)) {
		return PJ_EINVALIDOP;
	}

	status = pj_ice_strans_init_ice(agent.ice_strans, role, NULL, NULL);
	if (status != PJ_SUCCESS)
		agent_perror("error creating session", status);

	agent_reset_remote();

	pj_ice_strans_state state;
    do {
        pj_thread_sleep(100);
        if (!agent.ice_strans) {
            return -1;
        }

        state = pj_ice_strans_get_state(agent.ice_strans);
    } while (state == PJ_ICE_STRANS_STATE_READY);

	return status;
}

/*
 * Stop/destroy ICE session.
 */
void ice_destroy_session(void)
{
	pj_status_t status;

	if (agent.ice_strans == NULL) {
		logf("agent_destroy_session ice_strans == NULL");
		return;
	}

	if (!pj_ice_strans_has_sess(agent.ice_strans)) {
		logf("agent_destroy_session none");
		return;
	}

	status = pj_ice_strans_stop_ice(agent.ice_strans);
	if (status != PJ_SUCCESS)
		agent_perror("error stopping session", status);
	else
		logf("ICE session stopped");

	agent_reset_remote();

	agent.init = PJ_FALSE;
	agent.init_status = PJ_SUCCESS;
	agent.negotiation = PJ_FALSE;
	agent.negotiation_status = PJ_SUCCESS;
}

#define PRINT(...)	    \
		printed = pj_ansi_snprintf(p, maxlen - (p-buffer),  \
				__VA_ARGS__); \
				if (printed <= 0 || printed >= (int)(maxlen - (p-buffer))) \
				return -PJ_ETOOSMALL; \
				p += printed


/* Utility to create a=candidate SDP attribute */
static int print_cand(char buffer[], unsigned maxlen,
		const pj_ice_sess_cand *cand)
{
	char ipaddr[PJ_INET6_ADDRSTRLEN];
	char *p = buffer;
	int printed;

	PRINT("a=candidate:%.*s %u UDP %u %s %u typ ",
			(int)cand->foundation.slen,
			cand->foundation.ptr,
			(unsigned)cand->comp_id,
			cand->prio,
			pj_sockaddr_print(&cand->addr, ipaddr, sizeof(ipaddr), 0),
			(unsigned)pj_sockaddr_get_port(&cand->addr));

	PRINT("%s\n",
			pj_ice_get_cand_type_name(cand->type));

	if (p == buffer+maxlen)
		return -PJ_ETOOSMALL;

	*p = '\0';

	return (int)(p-buffer);
}

/* 
 * Encode ICE information in SDP.
 */
int ice_encode_session(char *buffer, unsigned maxlen)
{
	char *p = buffer;
	unsigned comp;
	int printed;
	pj_str_t local_ufrag, local_pwd;
	pj_status_t status;

	/* Write "dummy" SDP v=, o=, s=, and t= lines */
	PRINT("v=0\no=- 3414953978 3414953978 IN IP4 localhost\ns=ice\nt=0 0\n");

	/* Get ufrag and pwd from current session */
	pj_ice_strans_get_ufrag_pwd(agent.ice_strans, &local_ufrag, &local_pwd,
			NULL, NULL);

	/* Write the a=ice-ufrag and a=ice-pwd attributes */
	PRINT("a=ice-ufrag:%.*s\na=ice-pwd:%.*s\n",
			(int)local_ufrag.slen,
			local_ufrag.ptr,
			(int)local_pwd.slen,
			local_pwd.ptr);

	/* Write each component */
	for (comp=0; comp<agent.opt.comp_cnt; ++comp) {
		unsigned j, cand_cnt;
		pj_ice_sess_cand cand[PJ_ICE_ST_MAX_CAND];
		char ipaddr[PJ_INET6_ADDRSTRLEN];

		/* Get default candidate for the component */
		status = pj_ice_strans_get_def_cand(agent.ice_strans, comp+1, &cand[0]);
		if (status != PJ_SUCCESS)
			return -status;

		/* Write the default address */
		if (comp==0) {
			/* For component 1, default address is in m= and c= lines */
			PRINT("m=audio %d RTP/AVP 0\n"
					"c=IN IP4 %s\n",
					(int)pj_sockaddr_get_port(&cand[0].addr),
					pj_sockaddr_print(&cand[0].addr, ipaddr,
							sizeof(ipaddr), 0));
		} else if (comp==1) {
			/* For component 2, default address is in a=rtcp line */
			PRINT("a=rtcp:%d IN IP4 %s\n",
					(int)pj_sockaddr_get_port(&cand[0].addr),
					pj_sockaddr_print(&cand[0].addr, ipaddr,
							sizeof(ipaddr), 0));
		} else {
			/* For other components, we'll just invent this.. */
			PRINT("a=Xice-defcand:%d IN IP4 %s\n",
					(int)pj_sockaddr_get_port(&cand[0].addr),
					pj_sockaddr_print(&cand[0].addr, ipaddr,
							sizeof(ipaddr), 0));
		}

		/* Enumerate all candidates for this component */
		cand_cnt = PJ_ARRAY_SIZE(cand);
		status = pj_ice_strans_enum_cands(agent.ice_strans, comp+1,
				&cand_cnt, cand);
		if (status != PJ_SUCCESS)
			return -status;

		/* And encode the candidates as SDP */
		for (j=0; j<cand_cnt; ++j) {
			printed = print_cand(p, maxlen - (unsigned)(p-buffer), &cand[j]);
			if (printed < 0)
				return -PJ_ETOOSMALL;
			p += printed;
		}
	}

	if (p == buffer+maxlen)
		return -PJ_ETOOSMALL;

	*p = '\0';
	return (int)(p - buffer);
}

static void agent_show_ice(void)
{
	static char buffer[1000];
	int len;

	if (agent.ice_strans == NULL) {
		return;
	}

	puts("General info");
	puts("---------------");
	printf("Component count    : %d\n", agent.opt.comp_cnt);
	printf("Status             : ");
	if (pj_ice_strans_sess_is_complete(agent.ice_strans))
		puts("negotiation complete");
	else if (pj_ice_strans_sess_is_running(agent.ice_strans))
		puts("negotiation is in progress");
	else if (pj_ice_strans_has_sess(agent.ice_strans))
		puts("session ready");
	else
		puts("session not created");

	if (!pj_ice_strans_has_sess(agent.ice_strans)) {
		puts("Create the session first to see more info");
		return;
	}

	printf("Negotiated comp_cnt: %d\n",
			pj_ice_strans_get_running_comp_cnt(agent.ice_strans));
	printf("Role               : %s\n",
			pj_ice_strans_get_role(agent.ice_strans)==PJ_ICE_SESS_ROLE_CONTROLLED ?
					"controlled" : "controlling");

	len = ice_encode_session(buffer, sizeof(buffer));
	if (len < 0) {
		puts("not enough buffer to show ICE status");
		return;
	}

	puts("");
	printf("Local SDP (paste this to remote host):\n"
			"--------------------------------------\n"
			"%s\n", buffer);


	puts("");
	puts("Remote info:\n"
			"----------------------");
	if (agent.remote.cand_cnt==0) {
		puts("No remote info yet");
	} else {
		unsigned i;

		printf("Remote ufrag       : %s\n", agent.remote.ufrag);
		printf("Remote password    : %s\n", agent.remote.pwd);
		printf("Remote cand. cnt.  : %d\n", agent.remote.cand_cnt);

		for (i=0; i<agent.remote.cand_cnt; ++i) {
			len = print_cand(buffer, sizeof(buffer), &agent.remote.cand[i]);
			if (len < 0) {
				puts("not enough buffer to show ICE status");
				return;
			}

			printf("  %s", buffer);
		}
	}
}

#if defined(_WIN32) || defined(_WIN64)
#define strtok_r(s,d,c) strtok_s(s,d,c)
#endif
/*
 * Input and parse SDP from the remote (containing remote's ICE information) 
 * and save it to global variables.
 */
pj_status_t agent_update_remote(const char *buffer)
{
	char linebuf[160];
	char *ctx = NULL;
	unsigned media_cnt = 0;
	unsigned comp0_port = 0;
	char     comp0_addr[80];
	pj_bool_t done = PJ_FALSE;
	char *p = strdup(buffer);

	agent_reset_remote();

	comp0_addr[0] = '\0';

	while (!done) {
		pj_size_t len;
		char *line;

		line = strtok_r(p, "\n", &ctx);
		p = NULL;
		if (line==NULL)
			break;

		strcpy(linebuf, line);
		len = strlen(linebuf);
		while (len && (linebuf[len-1] == '\r' || linebuf[len-1] == '\n'))
			linebuf[--len] = '\0';

		line = linebuf;
		while (len && pj_isspace(*line))
			++line, --len;

		if (len==0)
			break;

		/* Ignore subsequent media descriptors */
		if (media_cnt > 1)
			continue;

		switch (line[0]) {
		case 'm':
		{
			int cnt;
			char media[32], portstr[32];

			++media_cnt;
			if (media_cnt > 1) {
				puts("Media line ignored");
				break;
			}

			cnt = sscanf(line+2, "%s %s RTP/", media, portstr);
			if (cnt != 2) {
				errf("Error parsing media line");
				goto on_error;
			}

			comp0_port = atoi(portstr);
		}
		break;
		case 'c':
		{
			int cnt;
			char c[32], net[32], ip[80];

			cnt = sscanf(line+2, "%s %s %s", c, net, ip);
			if (cnt != 3) {
				errf("Error parsing connection line");
				goto on_error;
			}

			strcpy(comp0_addr, ip);
		}
		break;
		case 'a':
		{
			char *attr = strtok(line+2, ": \t\r\n");
			if (strcmp(attr, "ice-ufrag")==0) {
				strcpy(agent.remote.ufrag, attr+strlen(attr)+1);
			} else if (strcmp(attr, "ice-pwd")==0) {
				strcpy(agent.remote.pwd, attr+strlen(attr)+1);
			} else if (strcmp(attr, "rtcp")==0) {
				char *val = attr+strlen(attr)+1;
				int af, cnt;
				int port;
				char net[32], ip[64];
				pj_str_t tmp_addr;
				pj_status_t status;

				cnt = sscanf(val, "%d IN %s %s", &port, net, ip);
				if (cnt != 3) {
					errf("Error parsing rtcp attribute");
					goto on_error;
				}

				if (strchr(ip, ':'))
					af = pj_AF_INET6();
				else
					af = pj_AF_INET();

				pj_sockaddr_init(af, &agent.remote.def_addr[1], NULL, 0);
				tmp_addr = pj_str(ip);
				status = pj_sockaddr_set_str_addr(af, &agent.remote.def_addr[1],
						&tmp_addr);
				if (status != PJ_SUCCESS) {
					errf("Invalid IP address");
					goto on_error;
				}
				pj_sockaddr_set_port(&agent.remote.def_addr[1], (pj_uint16_t)port);

			} else if (strcmp(attr, "candidate")==0) {
				char *sdpcand = attr+strlen(attr)+1;
				int af, cnt;
				char foundation[32], transport[12], ipaddr[80], type[32];
				pj_str_t tmpaddr;
				int comp_id, prio, port;
				pj_ice_sess_cand *cand;
				pj_status_t status;

				cnt = sscanf(sdpcand, "%s %d %s %d %s %d typ %s",
						foundation,
						&comp_id,
						transport,
						&prio,
						ipaddr,
						&port,
						type);
				if (cnt != 7) {
					errf("error: Invalid ICE candidate line");
					goto on_error;
				}

				cand = &agent.remote.cand[agent.remote.cand_cnt];
				pj_bzero(cand, sizeof(*cand));

				if (strcmp(type, "host")==0)
					cand->type = PJ_ICE_CAND_TYPE_HOST;
				else if (strcmp(type, "srflx")==0)
					cand->type = PJ_ICE_CAND_TYPE_SRFLX;
				else if (strcmp(type, "relay")==0)
					cand->type = PJ_ICE_CAND_TYPE_RELAYED;
				else {
					errf("Error: invalid candidate type '%s'", type);
					goto on_error;
				}

				cand->comp_id = (pj_uint8_t)comp_id;
				pj_strdup2(agent.pool, &cand->foundation, foundation);
				cand->prio = prio;

				if (strchr(ipaddr, ':'))
					af = pj_AF_INET6();
				else
					af = pj_AF_INET();

				tmpaddr = pj_str(ipaddr);
				pj_sockaddr_init(af, &cand->addr, NULL, 0);
				status = pj_sockaddr_set_str_addr(af, &cand->addr, &tmpaddr);
				if (status != PJ_SUCCESS) {
					errf("Error: invalid IP address '%s'", ipaddr);
					goto on_error;
				}

				pj_sockaddr_set_port(&cand->addr, (pj_uint16_t)port);

				++agent.remote.cand_cnt;

				if (cand->comp_id > agent.remote.comp_cnt)
					agent.remote.comp_cnt = cand->comp_id;
			}
		}
		break;
		}
	}

	if (agent.remote.cand_cnt==0 || agent.remote.ufrag[0]==0
			|| agent.remote.pwd[0]==0 || agent.remote.comp_cnt == 0) {
		errf("Error: not enough info");
		goto on_error;
	}

	if (comp0_port==0 || comp0_addr[0]=='\0') {
		errf("Error: default address for component 0 not found");
		goto on_error;
	} else {
		int af;
		pj_str_t tmp_addr;
		pj_status_t status;

		if (strchr(comp0_addr, ':'))
			af = pj_AF_INET6();
		else
			af = pj_AF_INET();

		pj_sockaddr_init(af, &agent.remote.def_addr[0], NULL, 0);
		tmp_addr = pj_str(comp0_addr);
		status = pj_sockaddr_set_str_addr(af, &agent.remote.def_addr[0],
				&tmp_addr);
		if (status != PJ_SUCCESS) {
			errf("Invalid IP address in c= line");
			goto on_error;
		}
		pj_sockaddr_set_port(&agent.remote.def_addr[0], (pj_uint16_t)comp0_port);
	}

	logf("Done, %d remote candidate(s) added", agent.remote.cand_cnt);

	free(p);
	return PJ_SUCCESS;

	on_error:
	agent_reset_remote();
	free(p);
	return PJ_EINVAL;
}

/*
 * Start ICE negotiation! This function is invoked from the menu.
 */
static pj_status_t agent_start_negotiation(void)
{
	pj_str_t rufrag, rpwd;
	pj_status_t status;

	if (agent.ice_strans == NULL) {
		return PJ_EINVALIDOP;
	}

	if (!pj_ice_strans_has_sess(agent.ice_strans)) {
		return PJ_EINVALIDOP;
	}

	if (agent.remote.cand_cnt == 0) {
		return PJ_EINVALIDOP;
	}

	logf("Starting ICE negotiation..");

	status = pj_ice_strans_start_ice(agent.ice_strans,
			pj_cstr(&rufrag, agent.remote.ufrag),
			pj_cstr(&rpwd, agent.remote.pwd),
			agent.remote.cand_cnt,
			agent.remote.cand);
	if (status != PJ_SUCCESS)
		agent_perror("Error starting ICE", status);
	else {
		logf("ICE negotiation started");

		ice_route_init();
	}

	return status;
}

/*
 * Send application data to remote agent.
 */
static pj_status_t agent_send_data(unsigned comp_id, const char *data, int len)
{
	pj_status_t status;

	if (agent.ice_strans == NULL) {
		return PJ_EINVALIDOP;
	}

	if (!pj_ice_strans_has_sess(agent.ice_strans)) {
		return PJ_EINVALIDOP;
	}

	if (!pj_ice_strans_sess_is_complete(agent.ice_strans)) {
		return PJ_EPENDING;
	}

	if (comp_id<1||comp_id>pj_ice_strans_get_running_comp_cnt(agent.ice_strans)) {
		return PJ_EINVAL;
	}

	status = pj_ice_strans_sendto(agent.ice_strans, comp_id, data, len,
			&agent.remote.def_addr[comp_id-1],
			pj_sockaddr_get_len(&agent.remote.def_addr[comp_id-1]));
	if (status != PJ_SUCCESS)
		agent_perror("Error sending data", status);
	else
		debugf("ICE Data sent:%d", len);

	return status;
}

int ice_sendto(const char *data, int len)
{
	//if (agent.negotiation_status == PJ_SUCCESS) {
		pj_status_t status = agent_send_data(1, data, len);
		if (status != PJ_SUCCESS) {
			return -status;
		}

		return len;
	//}
	//return -1;
}

int ice_master_start_negotiation(
		vpn_ctx_t *ctx,
		const char *peerSDP)
{
	pj_ice_strans_state state;
	pj_status_t status;

	state = pj_ice_strans_get_state(agent.ice_strans);
	const char *state_name = pj_ice_strans_state_name(state);

	debugf("ice_master_start_negotiation, state = %s", state_name);

	if (state == PJ_ICE_STRANS_STATE_RUNNING) {
		// drop or skip?
		debugf("ice_master_start_negotiation already success, skip!");

		control_req(CTRL_UNINIT_ICE, NULL);
		control_req(CTRL_INIT_ICE, NULL);

		return 0;
	}

	status = agent_update_remote(peerSDP);
	CHECK(status);

	agent_show_ice();

	status = agent_start_negotiation();
	CHECK(status);

	return 0;
}

int ice_slave_start_negotiation(vpn_ctx_t *ctx, const char *peerSDP)
{
	pj_ice_strans_state state;
	pj_status_t status;

	state = pj_ice_strans_get_state(agent.ice_strans);
	const char * state_name = pj_ice_strans_state_name(state);

	debugf("ice_slave_start_negotiation, state = %s", state_name);

	if (state == PJ_ICE_STRANS_STATE_RUNNING) {
		debugf("ice_slave_start_negotiation already success, skip!");

		control_req(CTRL_UNINIT_ICE, NULL);
		control_req(CTRL_INIT_ICE, NULL);

		return 0;
	}

	status = agent_update_remote(peerSDP);
	if (status!=PJ_SUCCESS) {
		agent_perror("agent_update_remote", status);
		return status;
	}

	agent_show_ice();

	status = agent_start_negotiation();
	if (status!=PJ_SUCCESS) {
		agent_perror("agent_start_negotiation", status);
		return status;
	}

	return 0;
}

int ice_get_state()
{
	pj_ice_strans_state state = pj_ice_strans_get_state(agent.ice_strans);
	const char *state_name = pj_ice_strans_state_name(state);
	debugf("ice_get_state, state = %s", state_name);
	return state;
}

int ice_packet_is_ping(const char *buf, long size)
{
	if (size >= sizeof(int)) {
		if (*(int *)buf == 0x2198f219) {
			return 1;
		}
	}
	return 0;
}

int ice_packet_ping()
{
	int packet = 0x2198f219;
	if (ice_sendto((const char *)&packet, sizeof(packet)) < 0) {
		return 1;
	}

	return 0;
}

int ice_packet_pong()
{
	int packet = 0x838fce68;
	if (ice_sendto((const char *)&packet, sizeof(packet)) < 0) {
		return 1;
	}

	return 0;
}

int ice_keepalive()
{
	if (time(NULL) - agent.last_recv > 30) {
		ice_packet_ping();
		return 1;
	}
	return 0;
}

int ice_is_interrupt()
{
	if (time(NULL) - agent.last_recv > 60) {
		return 1;
	}
	return 0;
}

static void ice_route_init()
{
	if (route_queue != NULL)
		HXdeque_free(route_queue);
	route_queue = HXdeque_init();

	if (agent.vpn_ctx->args->mode == SHADOWVPN_MODE_CLIENT) {
		int gw = ifc_get_default_route(agent.vpn_ctx->args->wan_intf);
		if (gw == 0) {
			errf("ice_route_init: no default gw for %s", agent.vpn_ctx->args->wan_intf);
			return;
		}

		for (int comp=0; comp<agent.opt.comp_cnt; ++comp) {
			unsigned j, cand_cnt;
			pj_ice_sess_cand cand[PJ_ICE_ST_MAX_CAND];
			char ipaddr[PJ_INET6_ADDRSTRLEN];
			pj_status_t status;

			/* Enumerate all candidates for this component */
			cand_cnt = PJ_ARRAY_SIZE(cand);
			status = pj_ice_strans_enum_cands(agent.ice_strans, comp+1,
					&cand_cnt, cand);
			if (status != PJ_SUCCESS)
				return;


			for (j=0; j<cand_cnt; ++j) {
				if (cand[j].type == PJ_ICE_CAND_TYPE_SRFLX ||
						cand[j].type == PJ_ICE_CAND_TYPE_PRFLX ||
						cand[j].type == PJ_ICE_CAND_TYPE_RELAYED) {

					struct route_t *new_route = (struct route_t *)malloc(sizeof(struct route_t));
					if (new_route != NULL) {
						new_route->intf = agent.vpn_ctx->args->wan_intf;
						new_route->target_ip = cand[j].addr;
						new_route->prefix_length = 32;
						new_route->gateway_ip = *(struct in_addr *)&gw;
						HXdeque_push(route_queue, new_route);
					}
				}
			}
		}

		for (int i = 0; i < agent.remote.cand_cnt; i++) {
			if (agent.remote.cand[i].type == PJ_ICE_CAND_TYPE_SRFLX ||
					agent.remote.cand[i].type == PJ_ICE_CAND_TYPE_PRFLX ||
					agent.remote.cand[i].type == PJ_ICE_CAND_TYPE_RELAYED) {

				struct route_t *new_route = (struct route_t *)malloc(sizeof(struct route_t));
				if (new_route != NULL) {
					new_route->intf = agent.vpn_ctx->args->wan_intf;
					new_route->target_ip = agent.remote.cand[i].addr;
					new_route->prefix_length = 32;
					new_route->gateway_ip = *(struct in_addr *)&gw;
					HXdeque_push(route_queue, new_route);
				}
			}
		}
	}

	// apply routes
	const struct HXdeque_node *node;
	for (node = route_queue->first; node != NULL; node = node->next) {
		const struct route_t *route = (struct route_t *)node->ptr;
		char ipaddr[PJ_INET6_ADDRSTRLEN];
		pj_sockaddr_print(&route->target_ip, ipaddr, sizeof(ipaddr), 0);
		ifc_add_route(
				route->intf,
				ipaddr,
				route->prefix_length,
				inet_ntoa(route->gateway_ip));
	}
}

static void ice_route_uninit()
{
	if (route_queue == NULL)
		return;

	struct route_t *route;
	while ((route = HXdeque_shift(route_queue))) {
		char ipaddr[PJ_INET6_ADDRSTRLEN];
		pj_sockaddr_print(&route->target_ip, ipaddr, sizeof(ipaddr), 0);
		ifc_remove_route(
				route->intf,
				ipaddr,
				route->prefix_length,
				inet_ntoa(route->gateway_ip));
		free(route);
	}
}

int agent_do_init(vpn_ctx_t *ctx)
{
	return agent_init_instance();
}

void agent_do_cleanup(vpn_ctx_t *ctx)
{
	sess_talking_stop(agent.vpn_ctx);

	ice_route_uninit();

	ice_destroy_session();
	agent_destroy_instance();
}

int ice_init(vpn_ctx_t *ctx)
{
	pj_status_t status = agent_init(
			ctx,
			ctx->args->turn_server, ctx->args->turn_server_port,
			ctx->args->turn_user, ctx->args->turn_password);
	if (status != PJ_SUCCESS) {
		agent_perror("agent_init", status);
		return status;
	}

	control_init(ctx);
	control_req(CTRL_INIT_ICE, NULL);

	sess_talking_stop(agent.vpn_ctx);
	sess_taking_init(ctx);

	if (!pj_thread_is_registered()) {
		pj_thread_desc *thread_desc = (pj_thread_desc*)malloc(sizeof(pj_thread_desc));
		pj_thread_t *thread;
		pj_bzero(thread_desc, sizeof(pj_thread_desc));
		pj_thread_register("ice_daemon", *thread_desc, &thread);
	}

	return 0;
}

void ice_cleanup(vpn_ctx_t *ctx)
{
	control_cleanup();
	agent_do_cleanup(ctx);
	agent_destroy();
}
