
#include <string.h>

#include <pjlib.h>
#include <pjlib-util.h>
#include <pjnath.h>

#include <pthread.h>
#include <libHX/defs.h>
#include <libHX/deque.h>

#include "vpn.h"
#include "control.h"
#include "iceagent.h"
#include "session.h"

#include "log.h"

struct cmd_t {
	enum CONTROL_t type;
	void *data;
};

static struct control_t {
	vpn_ctx_t *cmd_ctx;
	struct HXdeque *cmd_queue;
	int cmd_exit;
	pthread_attr_t cmd_attr;
	pthread_mutex_t cmd_mutex;
	pthread_cond_t cmd_cond;
	pthread_t cmd_thread;
	int cmd_last;
} control;


static void control_on_req(struct cmd_t *cmd);

static void * cmd_handler(void *data)
{
	pj_thread_desc *thread_desc = (pj_thread_desc*)malloc(sizeof(pj_thread_desc));
	pj_thread_t *thread;
	pj_bzero(thread_desc, sizeof(pj_thread_desc));
	pj_thread_register("ice_daemon", *thread_desc, &thread);

	struct cmd_t *cmd = NULL;
	while (!control.cmd_exit) {
		pthread_mutex_lock(&control.cmd_mutex);
		cmd = HXdeque_shift(control.cmd_queue);
		pthread_mutex_unlock(&control.cmd_mutex);

		pthread_mutex_lock(&control.cmd_mutex);
		if (cmd == NULL) {
			pthread_cond_wait(&control.cmd_cond, &control.cmd_mutex);
		}
		pthread_mutex_unlock(&control.cmd_mutex);

		if (cmd != NULL) {
			control_on_req(cmd);
			free(cmd);
		}
	}

	debugf("cmd_handler exit");

	return NULL;
}

void control_init(vpn_ctx_t *ctx)
{
	control.cmd_ctx = ctx;
	control.cmd_queue = HXdeque_init();
	control.cmd_exit = 0;
	control.cmd_last = -1;

	pthread_attr_init(&control.cmd_attr);
	pthread_attr_setdetachstate(&control.cmd_attr,PTHREAD_CREATE_DETACHED);

	pthread_mutex_init(&control.cmd_mutex, NULL);
	pthread_create(&control.cmd_thread, &control.cmd_attr, cmd_handler, NULL);
}

void control_cleanup()
{
	control.cmd_exit = 1;

	control_req(CTRL_UNKNOWN, NULL);
	sleep(2);

	if (control.cmd_queue != NULL)
		HXdeque_free(control.cmd_queue);

	control.cmd_queue = NULL;

	pthread_mutex_destroy(&control.cmd_mutex);
	pthread_attr_destroy(&control.cmd_attr);
}

static void control_on_req(struct cmd_t *cmd)
{
	debugf("control_on_req, type=%d, last=%d", cmd->type, control.cmd_last);

	switch (cmd->type) {
	case CTRL_INIT_ICE:
		if (control.cmd_last == -1) {
			if (agent_do_init(control.cmd_ctx) != 0) {
				errf("agent_do_init fail & exit");
				exit(7);
			}
			control.cmd_last = cmd->type;
		}
		break;

	case CTRL_INIT_ICE_SESS:
		control.cmd_last = cmd->type;
		break;

	case CTRL_ICE_NEGO:
		if (control.cmd_ctx->args->mode == SHADOWVPN_MODE_SERVER) {
			if (ice_master_start_negotiation(
					control.cmd_ctx,
					(const char *)cmd->data) == 0) {
				control.cmd_last = cmd->type;
			}
			else {
				control_req(CTRL_UNINIT_ICE, NULL);
				control_req(CTRL_INIT_ICE, NULL);
			}
		}
		else {
			if (ice_slave_start_negotiation(
					control.cmd_ctx,
					(const char *)cmd->data) == 0) {
				control.cmd_last = cmd->type;
			}
			else {
				control_req(CTRL_UNINIT_ICE, NULL);
				control_req(CTRL_INIT_ICE, NULL);
			}
		}
		break;
	case CTRL_ICE_NEGO_DONE:
		control.cmd_last = cmd->type;
		break;

	case CTRL_REQ_CONN:
		ice_keepalive();

		if (control.cmd_ctx->args->mode == SHADOWVPN_MODE_SERVER) {

		}
		else {
			if (ice_get_state() != PJ_ICE_STRANS_STATE_RUNNING &&
					ice_is_interrupt()) {
				if (control.cmd_last >= CTRL_INIT_ICE && control.cmd_last < CTRL_ICE_NEGO_DONE) {
					sess_slave_offer(control.cmd_ctx);
					control.cmd_last = cmd->type;
				}
				else {
					if (control.cmd_last < CTRL_ICE_NEGO_DONE) {
						control_req(cmd->type, NULL);
					}
				}
			}
		}
		break;

	case CTRL_ACK_CONN:
		sess_master_answer(
				control.cmd_ctx->args);
		control_req(CTRL_ICE_NEGO, strdup((const char *)cmd->data));
		control.cmd_last = cmd->type;
		break;

	case CTRL_ACK_ACCEPT:
		control_req(CTRL_ICE_NEGO, strdup((const char *)cmd->data));
		control.cmd_last = cmd->type;
		break;

	case CTRL_UNINIT_ICE_SESS:
		control.cmd_last = cmd->type;
		break;

	case CTRL_UNINIT_ICE:
		agent_do_cleanup(control.cmd_ctx);
		control.cmd_last = -1;
		break;

	default:
		break;
	}

	if (cmd->data) {
		free(cmd->data);
	}
}

void control_req(int cmd, void *data)
{
	debugf("control_req, type=%d", cmd);

	pthread_mutex_lock(&control.cmd_mutex);

	struct cmd_t *new_cmd = (struct cmd_t *)malloc(sizeof(struct cmd_t));
	if (new_cmd != NULL) {
		new_cmd->type = cmd;
		new_cmd->data = data;
		HXdeque_push(control.cmd_queue, new_cmd);
	}

	pthread_cond_signal(&control.cmd_cond);

	pthread_mutex_unlock(&control.cmd_mutex);
}
