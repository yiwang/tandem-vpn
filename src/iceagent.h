/*
 * iceagent.h
 *
 *  Created on: 2015年5月22日
 *      Author: Yi
 */

#ifndef SRC_ICEAGENT_H_
#define SRC_ICEAGENT_H_

int ice_init(vpn_ctx_t *ctx);
void ice_cleanup(vpn_ctx_t *ctx);
int ice_get_state();

int agent_do_init(vpn_ctx_t *ctx);
void agent_do_cleanup(vpn_ctx_t *ctx);

int ice_create_session(int is_slave);
void ice_destroy_session(void);

int ice_sendto(const char *data, int len);
int ice_encode_session(char *buffer, unsigned maxlen);
int ice_master_start_negotiation(
		vpn_ctx_t *ctx,
		const char *peerSDP);
int ice_slave_start_negotiation(
		vpn_ctx_t *ctx, const char *peerSDP);


int ice_packet_is_ping(const char *buf, long size);
int ice_packet_ping();
int ice_packet_pong();
int ice_keepalive();
int ice_is_interrupt();

#endif /* SRC_ICEAGENT_H_ */
