/*
 * route.c
 *
 *  Created on: 2015年5月27日
 *      Author: Yi
 */

#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <net/route.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include "log.h"
#include "ifc.h"

int route(int add, const char *target, const char *gw, const char *mask)
{
	int sockfd;
	struct rtentry route;
	struct sockaddr_in *addr;
	int err = 0;

	debugf("route %s target=%s gw=%s mask=%s",
			add ? "add" : "delete", target, gw, mask);

	if ((sockfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0) {
		perror("socket");
		exit(1);
	}

	memset(&route, 0, sizeof(route));
	addr = (struct sockaddr_in*) &route.rt_gateway;
	addr->sin_family = AF_INET;
	addr->sin_addr.s_addr = inet_addr(gw);
	addr = (struct sockaddr_in*) &route.rt_dst;
	addr->sin_family = AF_INET;
	addr->sin_addr.s_addr = inet_addr(target);
	addr = (struct sockaddr_in*) &route.rt_genmask;
	addr->sin_family = AF_INET;
	addr->sin_addr.s_addr = inet_addr(mask);
	route.rt_flags = RTF_UP | RTF_GATEWAY;
	route.rt_metric = 0;
	if ((err = ioctl(sockfd, add ? SIOCADDRT : SIOCDELRT, &route)) != 0) {
		perror("SIOCADDRT failed");
		exit(1);
	}

	close(sockfd);
	return 0;
}


int get_default_gw(char gw[], int size)
{
	FILE *f;
	char line[100] , *p , *c, *g, *saveptr;
	int nRet=1;

	f = fopen("/proc/net/route" , "r");
	while (fgets(line , 100 , f)) {
		p = strtok_r(line , " \t", &saveptr);
		c = strtok_r(NULL , " \t", &saveptr);
		g = strtok_r(NULL , " \t", &saveptr);

		if(p!=NULL && c!=NULL) {
			if(strcmp(c , "00000000") == 0) {
				if (g) {
					char *pEnd;
					int ng=strtol(g,&pEnd,16);
					//ng=ntohl(ng);
					struct sockaddr_in addr;
					addr.sin_addr.s_addr=ng;
					//debugf("route dddddddddddddddddddddd:%x", inet_ntoa(addr.sin_addr.s_addr));
					//strncpy(gw, inet_ntoa(addr), size);
					nRet=0;
				}
				break;
			}
		}
	}
	fclose(f);

	return nRet;
}
