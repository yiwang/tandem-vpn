
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/queue.h>
#include <fcntl.h>
#include <unistd.h>
#include <pthread.h>

#include <strings.h> // fix bzero()
#include <pjlib.h>
#include <pjlib-util.h>
#include <pjnath.h>

#include "log.h"
#include "args.h"
#include "vpn.h"
#include "crypto.h"
#include "ifc.h"
#include "control.h"
#include "iceagent.h"
#include "session.h"


#ifndef TARGET_WIN32
static int max(int a, int b) {
	return a > b ? a : b;
}

static int min(int a, int b) {
	return a < b ? a : b;
}
#endif

typedef struct _session_message {
	char type;
	char flag;
	short dataLength;
	char deviceID[DEVICE_ID_LEN];
	char SDP[1];
} session_message;

struct session {
	pthread_attr_t 		attr;
	pthread_t 			thread;

	pj_bool_t           quit;
	pj_sock_t			sock;
	struct timeval 		last_recv;
	int 				negotiation_times;
} sess;

static void * sess_taking_proc(void *data);
static void sess_handle_timeout(vpn_ctx_t *ctx);
int sess_master_register(vpn_ctx_t *ctx);

int sess_taking_init(vpn_ctx_t *ctx)
{
	sess.quit = PJ_FALSE;
	sess.sock = -1;

	pthread_attr_init(&sess.attr);
	pthread_attr_setdetachstate(&sess.attr, PTHREAD_CREATE_DETACHED);

	pthread_create(
			&sess.thread, &sess.attr, &sess_taking_proc, ctx);

	return 0;
}

static int sess_on_recv(vpn_ctx_t *ctx, void *buf, size_t size)
{
	session_message *msg = (session_message*)buf;
	char sdp[MAX_SDP_LEN];
	int dataLen;
	pj_status_t status;

	debugf("sess_on_recv, size=%ld", size);

	if (size == 0) {
		return -1;
	}

	gettimeofday(&sess.last_recv, NULL);

	switch (msg->type) {
	case MSG_REGISTER:
		// act as a sesstion server
		break;

	case MSG_REGISTER_ACK:
		// act as a peer server
		// prepare for MSG_OFFER
		logf("Register to session server OK!");
		break;

	case MSG_KEEPALIVE:
		logf("recv MSG_KEEPALIVE");
		break;

	case MSG_OFFER:
		// act as a peer server
		logf("Offer message!");

		if (size <= sizeof(session_message)) {
			return -1;
		}

		dataLen = ntohs(msg->dataLength);
		if (dataLen > (size - 4)) {
			logf("bad MSG_OFFER message, ignored!");
			break;
		}

		if (memcmp(msg->deviceID, ctx->args->device_id, DEVICE_ID_LEN) != 0) {
			logf("Requestd device id mismatch with peer server!");
			break;
		}

//		char sdp[MAX_SDP_LEN];
//		ice_encode_session(sdp, MAX_SDP_LEN);
//
//		sess_master_answer(
//				ctx->args, sdp);

		logf("Starting ice server instance...");

		control_req(CTRL_ACK_CONN, strdup(msg->SDP));
		sess.negotiation_times++;
		break;

	case MSG_ANSWER:
		// act as a peer client

		logf("recv MSG_ANSWER");

		if (size <= sizeof(session_message)) {
			return -1;
		}

		dataLen = ntohs(msg->dataLength);
		if (dataLen > (size - 4)) {
			logf("bad MSG_ANSWER message, ignored!");
			break;
		}

		if (msg->flag != 0) {
			// Error answer
			break;
		}

		if (memcmp(msg->deviceID, ctx->args->device_id, DEVICE_ID_LEN) != 0) {
			logf("Requestd device id mismatch with peer client!");
			break;
		}

		logf("Starting ice slave instance...");

		control_req(CTRL_ACK_ACCEPT, strdup(msg->SDP));
		sess.negotiation_times++;
		break;

	case UNREGISTER:
		logf("recv UNREGISTER");
		break;

	default:
		errf("wrong message type:%d.", msg->type);
		return -1;
	}

	return 0;
}

static int sess_taking_proc_sub(vpn_ctx_t *ctx)
{
	const char *host = ctx->args->session_server;
	int port = ctx->args->session_server_port;
	int sock;
	char msgbuffer[sizeof(session_message) + MAX_SDP_LEN];

	logf("Connect to session server %s:%d.", host, port);

	struct sockaddr_in remote_addr;
	sock = socket(PF_INET, SOCK_STREAM, 0);
	if (sock == -1) {
		return -1;
	}

	memset(&remote_addr,0,sizeof(remote_addr));
	remote_addr.sin_family = AF_INET;
	remote_addr.sin_addr.s_addr = inet_addr(host);
	remote_addr.sin_port = htons(port);

	if (connect(sock, (struct sockaddr *)&remote_addr, sizeof(remote_addr)) != 0) {
		// TODO: fix error handling
		logf("Connect to session server %s:%d. failed", host, port);
		close(sock);
		return -1;
	}

#ifdef WIN32
	unsigned long mode = 1;
	(ioctlsocket(sock, FIONBIO, &mode) == 0);
#else
	int flags = fcntl(sock, F_GETFL, 0);
	if (flags < 0) return -1;
	flags = flags | O_NONBLOCK;
	fcntl(sock, F_SETFL, flags);
#endif

	sess.sock = sock;

	if (ctx->args->mode == SHADOWVPN_MODE_SERVER) {
		sess_master_register(ctx);
	}

	while (!sess.quit) {
		fd_set rfds;
		FD_ZERO(&rfds);
		FD_SET(sess.sock, &rfds);
		int max_fd = sess.sock;

#ifndef TARGET_WIN32
		FD_SET(ctx->control_pipe[0], &rfds);
		max_fd = max(ctx->control_pipe[0], max_fd);
#else
		FD_SET(ctx->control_fd, &rfds);
		max_fd = max(ctx->control_fd, max_fd);
#endif
		max_fd++;

		int rc;
		struct timeval timeout;
		timerclear(&timeout);
		timeout.tv_sec = 30;

		debugf("wait for command");

		rc = select(max_fd, &rfds, NULL, NULL, &timeout);
		if (rc < 0) {
			perror("select");
			close(sock);
			return -1;
		}

		if (rc == 0) { // Timeout
			// send keep alive
			sess_handle_timeout(ctx);
		}

#ifndef TARGET_WIN32
		if (FD_ISSET(ctx->control_pipe[0], &rfds)) {
			char pipe_buf;
			if (read(ctx->control_pipe[0], &pipe_buf, 1)) {
			}
			continue;
		}
#else
		if (FD_ISSET(ctx->control_fd, &rfds)) {
			char buf;
			recv(ctx->control_fd, &buf, 1, 0);
			continue;
		}
#endif

		if (FD_ISSET(sess.sock, &rfds)) {
			rc = recv(sess.sock, msgbuffer, sizeof(msgbuffer), MSG_WAITALL);
			if (rc <= 0) {
				perror("recv break");
				close(sock);
				return -1;
			}
			sess_on_recv(ctx, msgbuffer, rc);
		}
	}

	debugf("sess_taking_proc end");

	return 0;
}

static void * sess_taking_proc(void *data)
{
		pj_thread_desc *thread_desc = (pj_thread_desc*)malloc(sizeof(pj_thread_desc));
		pj_thread_t *thread;
		pj_bzero(thread_desc, sizeof(pj_thread_desc));
		pj_thread_register("ice_daemon", *thread_desc, &thread);

	vpn_ctx_t *ctx = (vpn_ctx_t *)data;
	sess.negotiation_times = 0;
	while (!sess.quit) {
		sess_taking_proc_sub(ctx);
		sleep(3);
	}

	return NULL;
}

int sess_talking_stop(vpn_ctx_t *ctx)
{
	logf("sess_talking_reset");

	if (!sess.quit) {
		return -1;
	}
	sess.quit = PJ_TRUE;
	char buf = 0;

#ifndef TARGET_WIN32
	if (-1 == write(ctx->control_pipe[1], &buf, 1)) {
		err("write");
		return -1;
	}
#else
	int send_sock;
	struct sockaddr addr;
	socklen_t addrlen;
	if (-1 == (send_sock = vpn_udp_alloc(0, TUN_DELEGATE_ADDR, 0, &addr,
			&addrlen))) {
		errf("failed to init control socket");
		return -1;
	}
	if (-1 == sendto(send_sock, &buf, 1, 0, &agent.vpn_ctx->control_addr,
			agent.vpn_ctx->control_addrlen)) {
		err("sendto");
		close(send_sock);
		return -1;
	}
	close(send_sock);
	WaitForSingleObject(ctx->cleanEvent, INFINITE);
	CloseHandle(ctx->cleanEvent);
#endif

	return 0;
}

static int tunnel_client_offer(
		const tandemvpn_options_t *opts,
		const char *localSDP)
{
	char msgbuffer[sizeof(session_message) + MAX_SDP_LEN];
	int localSDPLen;
	int dataLen;
	int sess_slave_start_negotiation;
	char *buf;
	int rc = -1;

	session_message *msg = (session_message*)msgbuffer;

	localSDPLen = strnlen(localSDP, MAX_SDP_LEN);
	if (localSDPLen >= MAX_SDP_LEN)
		return -1;

	if (strlen(opts->device_id) != DEVICE_ID_LEN)
		return -1;

	memset(msgbuffer, 0, sizeof(msgbuffer));
	msg->type = MSG_OFFER;
	msg->flag = 0;
	dataLen = DEVICE_ID_LEN + localSDPLen + 1;
	msg->dataLength = htons(dataLen);
	memcpy(msg->deviceID, opts->device_id, DEVICE_ID_LEN);
	memcpy(msg->SDP, localSDP, localSDPLen + 1);
	sess_slave_start_negotiation = 0;
	dataLen += 4;
	buf = msgbuffer;
	while (sess_slave_start_negotiation < dataLen) {
		rc = send(sess.sock, buf + sess_slave_start_negotiation, dataLen - sess_slave_start_negotiation, MSG_WAITALL);
		if (rc < 0) {
			rc = -1;
			break;
		}

		sess_slave_start_negotiation += rc;
	}

	return rc;
}

int sess_slave_offer(vpn_ctx_t *ctx)
{
	debugf("sess_slave_offer");

	pj_status_t status;
	char sdp[MAX_SDP_LEN];

	ice_encode_session(sdp, MAX_SDP_LEN);
	tunnel_client_offer(ctx->args, sdp);

	return 0;
}

int sess_master_answer(
		const tandemvpn_options_t *opts)
{
	debugf("sess_master_answer");

	char msgbuffer[sizeof(session_message) + MAX_SDP_LEN];
	session_message *msg = (session_message*)msgbuffer;
	int dataLen;
	int SDPLen;
	int bytesSent;
	char *buf;
	int rc;

	char sdp[MAX_SDP_LEN];
	ice_encode_session(sdp, MAX_SDP_LEN);

	SDPLen = strlen(sdp) + 1;

	memset(msgbuffer, 0, sizeof(msgbuffer));
	msg->type = MSG_ANSWER;
	dataLen = DEVICE_ID_LEN + SDPLen;
	msg->dataLength = htons(dataLen);
	memcpy(msg->deviceID, opts->device_id, DEVICE_ID_LEN);
	if (SDPLen)
		memcpy(msg->SDP, sdp, strlen(sdp));
	else
		msg->flag = 2; // device error

	bytesSent = 0;
	dataLen += 4;
	buf = msgbuffer;
	while (bytesSent < dataLen) {
		rc = send(sess.sock, buf + bytesSent, dataLen - bytesSent, MSG_WAITALL);
		if (rc <= 0) {
			return -1;
		}

		bytesSent += rc;
	}

	return 0;
}

int sess_master_register(vpn_ctx_t *ctx)
{
	debugf("sess_master_start");

	if (sess.sock == -1) {
		errf("sess_master_start failed");
		return -1;
	}

	int rc;
	char msgbuffer[sizeof(session_message) + MAX_SDP_LEN];
	session_message *msg = (session_message *)msgbuffer;

	msg->type = MSG_REGISTER;
	msg->flag = 0;
	msg->dataLength = htons(DEVICE_ID_LEN);
	memcpy(msg->deviceID, ctx->args->device_id, DEVICE_ID_LEN);
	rc = send(sess.sock, msgbuffer, DEVICE_ID_LEN + 4, MSG_WAITALL);
	if (rc <= 0) {
		errf("Failed register to session server, send error.");
		return -1;
	}

	return rc;
}

static void sess_handle_timeout(vpn_ctx_t *ctx)
{
	struct timeval now;
	struct timeval check_time;
	struct timeval check_interval;

	check_interval.tv_sec = 30;
	check_interval.tv_usec = 0;
	timeradd(&sess.last_recv, &check_interval, &check_time);

	gettimeofday(&now, NULL);
	if (timercmp(&now, &check_time, >)) {
		debugf("sess_handle_timeout");

		if (ctx->args->mode == SHADOWVPN_MODE_SERVER) {
			if (sess.sock == -1) {
				debugf("sess_handle_timeout exit because of not init agent.negotiation_sock");
				return;
			}

			char msgbuffer[sizeof(session_message) + MAX_SDP_LEN];
			session_message *msg = (session_message *)msgbuffer;

			/* send keep-alive */
			msg->type = MSG_KEEPALIVE;
			msg->flag = 0;
			msg->dataLength = htons(0);

			int rc = send(sess.sock, msgbuffer, 4, MSG_WAITALL);
			if (rc <= 0) {
				logf("Tunnel daemon update keep-alive failed, retry later.");
			}
		}
		else {
			control_req(CTRL_REQ_CONN, NULL);
		}
	}
}

void sess_route_init(vpn_ctx_t *ctx)
{
	debugf("sess_route_init");

	if (ctx->args->mode == SHADOWVPN_MODE_CLIENT) {
		int gw = ifc_get_default_route(ctx->args->wan_intf);
		if (gw != 0) {
			ifc_add_route(ctx->args->wan_intf,
					ctx->args->session_server,
					32,
					inet_ntoa(*(struct in_addr *)&gw));
		}
	}
}

void sess_route_uninit(vpn_ctx_t *ctx)
{
	debugf("sess_route_uninit");

	if (ctx->args->mode == SHADOWVPN_MODE_CLIENT) {
		int gw = ifc_get_default_route(ctx->args->wan_intf);
		if (gw != 0) {
			ifc_remove_route(ctx->args->wan_intf,
					ctx->args->session_server,
					32,
					inet_ntoa(*(struct in_addr *)&gw));
		}
	}
}
