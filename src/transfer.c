/*
 * trans.c
 *
 *  Created on: 2015年5月25日
 *      Author: Yi
 */

/* Returns 0 on success, -1 on error */
static int tunnel_say_hello(const vpn_ctx_t *ctx)
{
    int rc;
    int retry;

    struct sockaddr_storage fromaddr;
    int addrlen;

    struct timeval timeout;

    fd_set rfds;

    uint16_t rid;

    struct message_holder _holder;
    message *msg = (message *)&_holder;


    for (retry = 0; retry < 10; retry++) {
        log_info("Hello to server.");

        rid = tunnel_next_rid(t);
        rc = tunnel_send_hello(t, rid, NULL, 0);
        if (rc <= 0) {
            log_warning("Send hello to server, failed.");
            continue;
        }

        timerclear(&timeout);
        timeout.tv_sec = 1;
        FD_ZERO(&rfds);
        FD_SET(t->sock, &rfds);

        rc = select(FD_SETSIZE, &rfds, NULL, NULL, &timeout);
        if (rc <= 0) { // Timeout or error
            if (errno != EINTR)
                log_warning("Receive hello ack from server timeout.");
            continue;
        }

        addrlen = sizeof(fromaddr);
        message_init0(msg, 0, 0, 0, TUNNEL_MAX_DATA_LEN);
        rc = tunnel_receive_message(t, msg,
                                    (struct sockaddr *)&fromaddr,
                                    &addrlen);
        if (rc <= 0) {
            log_warning("Receive hello ack from %s error.",
                        AN(&fromaddr));
            continue;
        }

        if (message_length(msg) != rc
                || message_type(msg) != MSG_TYPE_HELLO_ACK
                || message_rid(msg) != rid) {
            log_warning("Receive unexpected message from %s, ignore.",
                        AN(&fromaddr));
            continue;
        }

        rc = tunnel_send_hello_ack(t, rid, NULL, 0);
        if (rc <= 0) {
            log_error("Send hello ack to server failed.");
            continue;
        }

        return 0;
    }

    return -1;
}
