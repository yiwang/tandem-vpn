/*
 * udptunnel : Tunnels TCP over UDP
 *
 * Copyright (C) 2014 Jingyu jingyu.niu@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef __SOCKET_H__
#define __SOCKET_H__

#if !defined(_WIN32) && !defined(_WIN64)
#include <inttypes.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netdb.h>
#else
#include <winsock2.h>
#include <ws2tcpip.h>
#endif /* WIN32 */

/* for Winsock compatible */
#if !defined(_WIN32) && !defined(_WIN64)
typedef int	SOCKET;
#define INVALID_SOCKET		-1
#endif

SOCKET socket_create(int family, int type, 
                     const char *host, const char *service);
SOCKET socket_connect(int family, int type, 
                      const char *host, const char *service);
SOCKET socket_connect_addr(const struct addrinfo *ai);

int socket_close(SOCKET s);

const char *socket_addr_name(const struct sockaddr *addr);
const char *socket_local_name(SOCKET sock);
const char *socket_remote_name(SOCKET sock);

#define AN(addr)     socket_addr_name((const struct sockaddr *)addr)
#define CSA(addr)    (const struct sockaddr *)(addr)

#endif /* __SOCKET_H__ */
