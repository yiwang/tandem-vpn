/*
 * control.h
 *
 *  Created on: 2015年6月1日
 *      Author: Yi
 */

#ifndef SRC_CONTROL_H_
#define SRC_CONTROL_H_

enum CONTROL_t {
	CTRL_INIT_ICE,			// create ice_strans
	CTRL_INIT_ICE_SESS,	    // create ICE session
	CTRL_REQ_CONN,
	CTRL_ACK_CONN,
	CTRL_ACK_ACCEPT,
	CTRL_ICE_NEGO,			// start ICE negotiation
	CTRL_ICE_NEGO_DONE,
	CTRL_UNINIT_ICE_SESS,	// destroy the ICE session
	CTRL_UNINIT_ICE,		// destroy ice_strans
	CTRL_UNKNOWN,
};

void control_init(vpn_ctx_t *ctx);
void control_cleanup();
void control_req(int cmd, void *data);

#endif /* SRC_CONTROL_H_ */
